# Awesome Tailwind CSS for beginners [![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

> A curated list of awesome things related to Tailwind CSS


## Resources

- [Tailwind Official Site](https://tailwindcss.com)
- [Try out Tailwind online](https://gustscss.com/)
- [Find premade components (like navbars, cards, etc.)](https://tailwindcomponents.com)
- [Cheat Sheet to look up classes quickly](http://nerdcave.com/tailwind-cheat-sheet)


## Tutorials

- [Tailwind CSS - Building a Login Page](https://mustafaaloko.github.io/2017/tailwind-css-building-a-login-page/)
- [Rebuilding Laravel.io with Tailwind CSS](https://www.youtube.com/watch?v=ZrRRMBaz5Z0)
- [Rebuilding Coinbase with Tailwind CSS](https://www.youtube.com/watch?v=7gX_ApBeSpQ) - [Codepen](https://codepen.io/adamwathan/pen/RxWrZr)
- [Rebuilding FreshBooks with Tailwind CSS](http://joey.io/rebuilding-freshbooks-with-tailwind-css/)
- [Create a Sticky Footer Using Tailwind CSS](https://jimshannon.me/blog/create-a-sticky-footer-using-tailwind-css)
- [Collection of marketing site templates](https://github.com/colmtuite/tailwind-template)
- [Rebuilding Twitter with Tailwind CSS](https://www.youtube.com/watch?v=Pg_5Ni1_bg4) - [CodePen](https://codepen.io/drehimself/full/vpeVMx)
- [Rebuilding Netlify with Tailwind CSS](https://www.youtube.com/watch?v=_JhTaENzfZQ&t=1263s)
- [Rebuilding Resolute](https://www.youtube.com/watch?v=banq3TfAPYk)

### Apps/Websites using TailwindCSS

- [Merched.com](https://merched.com)
- [NiftyCo](https://anifty.co)
- [Josh Manders](https://joshmanders.com/)
- [Matt Stauffer](https://mattstauffer.com/)
- [Miguel Piedrafita](https://miguelpiedrafita.com/)
- [Webslides](https://slides.zone)
- [Lichter.io (Alexander Lichter)](https://lichter.io)
- [Rias](https://rias.be)
- [Freek Van der Herten](https://murze.be/)
- [Taylor Bryant](https://taylorbryant.blog)
- [Oliver Davies](https://www.oliverdavies.uk)
- [BaseCode](https://basecodefieldguide.com)
- [Joey Beninghove](https://joey.io)
- [Stefan Bauer](https://stefanbauer.me)
- [Jim Shannon](https://jimshannon.me)
- [Eyewitness.io](https://eyewitness.io)
- [Laravel Spark](https://spark.laravel.com)
- [Snaptier](https://snaptier.co)
- [Milan Chheda](https://milanchheda.com)
- [Matheus Lima](https://matheuslima.com.br)
- [CSS Cursors](https://css-cursors.netlify.com/)
- [Developmint](https://developmint.de/)
- [Rational Investment Management](https://rationalim.com)
- [Stephen Popoola](https://stephenpopoola.uk)
- [Laraboost](https://laraboost.com)
- [Vince Mitchell](http://vincemitchell.me/)
- [Marco Mark](https://www.marcomark.net)
- [Nehal Hasnayeen](https://hasnayeen.github.io)
- [Plowman Craven](https://www.plowmancraven.co.uk)
- [Erik Campobadal](https://erik.cat)
- [Our Name is Mud](https://www.ournameismud.co.uk)
- [BudgetDuo](https://budgetduo.com)
- [Quickwords](https://quickwords.co)
- [Ben John Bagley](https://benbagley.co.uk)

## More resources
- [Awesome TailwindCSS](https://github.com/aniftyco/awesome-tailwindcss)

## License

[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](http://creativecommons.org/publicdomain/zero/1.0)
